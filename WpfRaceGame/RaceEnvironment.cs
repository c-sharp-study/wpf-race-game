﻿using System.Collections.Generic;
using System.Linq;

namespace WpfRaceGame
{
    /// <summary>
    /// 赛车环境类，给出当前比赛情况，用于判断车辆当前环境下是否能够触发技能
    /// </summary>
    public class RaceEnvironment
    {
        #region 公有属性
        /// <summary>
        /// 参赛车辆
        /// </summary>
        public List<BaseCar> CarList { get; set; }
        #endregion

        #region 公有方法

        /// <summary>
        /// 判断指定车辆是否为第最后一名
        /// </summary>
        /// <param name="car">需要判断的车辆</param>
        /// <returns></returns>
        public bool IsLast(BaseCar car)
        {
            return CarList.OrderByDescending((c) => c.XPos + c.Length).Last().Name == car.Name;

        }

        /// <summary>
        /// 判断指定车辆是否为第一名
        /// </summary>
        /// <param name="car">需要判断的车辆</param>
        /// <returns></returns>
        public bool IsFirst(BaseCar car)
        {
            return CarList.OrderByDescending((c) => c.XPos + c.Length).First().Name == car.Name;
        }

        /// <summary>
        /// 判断指定车辆是不是第一名
        /// </summary>
        /// <param name="car">需要判断的车辆</param>
        /// <returns></returns>
        public bool IsNotFirst(BaseCar car)
        {
            return CarList.OrderByDescending((c) => c.XPos + c.Length).First().Name != car.Name;
        }
        #endregion
    }
}