﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfRaceGame
{
    [ImplementPropertyChanged]
    public class BaseCar : INotifyPropertyChanged
    {

        /// <summary>
        /// 基本车的构造函数
        /// </summary>
        /// <param name="name">车名</param>
        /// <param name="length">车长</param>
        /// <param name="width">车宽</param>
        public BaseCar(string name, double length, double width)
        {
            Name = name;
            Length = length;
            Width = width;
        }

        public event PropertyChangedEventHandler PropertyChanged = (sendor, e) => { };


        # region 私有变量
        private double _length;
        #endregion

        #region 车的公有属性
        /// <summary>
        /// 车长
        /// </summary>
        public double Length {
            get
            {
                return _length;
            }
            set
            {
                // 三元表达式
                // 条件 ？真 ：假
                _length = value > 100 ? 100 : value;

                // if方法
                //if (value > 10)
                //{
                //    _length = 10;
                //}
                //else
                //{
                //    _length = value;
                //}
            }
        }

        /// <summary>
        /// 车宽
        /// </summary>
        public double Width { get; set; } = 50;

        /// <summary>
        /// 速度
        /// </summary>
        public double Speed { get; set; } = 0;

        /// <summary>
        /// 加速度
        /// </summary>
        public double Acc { get; set; }

        /// <summary>
        /// 最大速度
        /// </summary>
        public double MaxSpeed { get; set; }

        /// <summary>
        /// 车名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 能量，能量值>0才能使用技能
        /// </summary>
        public int Power { get; set; }

        /// <summary>
        /// 车当前所处的位置
        /// </summary>
        public double XPos { get; set; } = 0;
        #endregion

        # region 车的公有方法
        /// <summary>
        /// 带随机扰动的提速，但是不能超过最大速度
        /// </summary>
        public void SpeedUp()
        {
            double rand = new Random().NextDouble();
            Speed = Math.Min(Speed + Acc * rand, MaxSpeed);
        }

        /// <summary>
        /// 提速函数的重载, 使用提供的加速度进行加速
        /// </summary>
        /// <param name="acc">加速度</param>
        public void SpeedUp(double acc)
        {
            double rand = new Random().NextDouble();
            Speed = Math.Min(Speed + acc * rand, MaxSpeed);
        }

        /// <summary>
        /// 车的技能，基本车没有技能， virtual表示该方法可以被子类重写override
        /// </summary>
        public virtual void Skill(RaceEnvironment env)
        {
            
        }
        /// <summary>
        /// 让车移动,先提速在移动
        /// </summary>
        public void Move()
        {
            SpeedUp();
            // 同等于 XPos = XPos + Speed
            XPos += Speed;
        }
        # endregion
    }
}
