﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfRaceGame
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private BaseCar car1, car2, car3;
        private Ferrari redThunder;
        private List<BaseCar> carList;
        private RaceEnvironment env;
        public MainWindow()
        {
            InitializeComponent();
            car1 = new BaseCar("car1", 50, 50)
            {
                MaxSpeed = 5,
                Acc = 0.1
            };
            car2 = new BaseCar("car2", 60, 50)
            {
                MaxSpeed = 4,
                Acc = 0.2
            };
            car3 = new BaseCar("car3", 70, 50)
            {
                MaxSpeed = 10,
                Acc = 0.5
            };
            redThunder = new Ferrari("red", 20, 50, 0.3, 8)
            {
                Power = 10000
            };
            carList = new List<BaseCar>()
            {
                car1,
                car2,
                car3,
                redThunder
            };

            env = new RaceEnvironment()
            {
                CarList = carList
            };

            Rect1.DataContext = redThunder;
            Rect2.DataContext = car1;
            Rect3.DataContext = car2;
            Rect4.DataContext = car3;

            Task.Run(async () =>
            {
                while(true)
                {
                    await Task.Delay(60);
                    car1.Move();
                    car2.Move();
                    car3.Move();
                    redThunder.Move();
                    car1.Skill(env);
                    car2.Skill(env);
                    car3.Skill(env);
                    redThunder.Skill(env);
                    BaseCar car = carList.OrderByDescending((c) => c.XPos + c.Length).First();
                    if (car.XPos >= 600)
                    {
                        
                        MessageBox.Show("胜利！：" + car.Name);
                        break;
                    }
                }

            });
        }
    }
}
