﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfRaceGame
{
    public class Ferrari : BaseCar
    {
        private double _originalAcc;
        private double _originalMaxSpeed;

        /// <summary>
        /// 法拉利跑车的构造函数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="length"></param>
        /// <param name="width"></param>
        /// <param name="acc"></param>
        /// <param name="maxSpeed"></param>
        public Ferrari(string name, double length, double width, double acc, double maxSpeed) : base(name, length, width)
        {
            _originalAcc = acc;
            _originalMaxSpeed = maxSpeed;
            Acc = acc;
            MaxSpeed = maxSpeed;
        }

        /// <summary>
        /// 法拉利的技能，当法拉利是不是第一名且能量>0, 则加速度翻5倍，并提升最大速度上限10个点
        /// </summary>
        /// <param name="env">比赛环境</param>
        public override void Skill(RaceEnvironment env)
        {
            if (env.IsNotFirst(this) && Power > 0)
            {
                Acc = _originalAcc * 5;
                MaxSpeed = _originalMaxSpeed + 10;
                Power -= 1;
            }
            else
            {
                Acc = _originalAcc;
                MaxSpeed = _originalMaxSpeed;
            }
        }


    }
}
